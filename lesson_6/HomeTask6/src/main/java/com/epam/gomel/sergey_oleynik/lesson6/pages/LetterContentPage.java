package com.epam.gomel.sergey_oleynik.lesson6.pages;

import com.epam.gomel.sergey_oleynik.lesson6.reporting.Logger;
import com.epam.gomel.sergey_oleynik.lesson6.service.DataService;
import com.epam.gomel.sergey_oleynik.lesson6.utils.MyFileUtils;

public class LetterContentPage extends AbstractPage {

    public void downloadAttachFile() {
        browser.click(DataService.getDownloadFileLinkLocator());
        Logger.log.info("Download file '" + MyFileUtils.getDownloadFile().getName() + "' to directory '" + DataService.getDownloadDir() + "'");
    }
}