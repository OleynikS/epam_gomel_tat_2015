package com.epam.gomel.sergey_oleynik.lesson6.utils;

import com.epam.gomel.sergey_oleynik.lesson6.service.DataService;
import org.apache.commons.io.FileUtils;
import java.io.File;

public class MyFileUtils {

    private static final int LENGTH_FILE_RANDOM_NAME = 10;
    private static final int LENGTH_FILE_RANDOM_CONTENT = 100;
    private static String randomNameFile = MyRandomUtils.randomString(LENGTH_FILE_RANDOM_NAME) + ".txt";

    public static File createAttachFile() {

        File fileAttach = new File(DataService.getPathFileAttach() + randomNameFile);

        try {
        boolean exists = fileAttach.createNewFile();
        if (exists) {
            FileUtils.write(fileAttach, "File content "+MyRandomUtils.randomString(LENGTH_FILE_RANDOM_CONTENT));
            // log файл успешно создан
        }
        } catch (Exception e) {
    //      log
        }
        return fileAttach;
    }

    public static File getDownloadFile() {
        File fileDownload = new File(DataService.getDownloadDir() + randomNameFile);
        return fileDownload;
    }
}