package com.epam.gomel.sergey_oleynik.lesson6.test;

import com.epam.gomel.sergey_oleynik.lesson6.bo.common.Account;
import com.epam.gomel.sergey_oleynik.lesson6.bo.common.AccountBuilder;
import com.epam.gomel.sergey_oleynik.lesson6.bo.mail.MailLetter;
import com.epam.gomel.sergey_oleynik.lesson6.bo.mail.MailLetterBuilder;
import com.epam.gomel.sergey_oleynik.lesson6.service.LoginGuiService;
import com.epam.gomel.sergey_oleynik.lesson6.service.MailGuiService;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class MarkEmailAsNotSpamTest {
    private LoginGuiService loginGuiService = new LoginGuiService();
    private Account userAccount = AccountBuilder.getAccount();
    private MailGuiService mailGuiService = new MailGuiService();
    private MailLetter letter = MailLetterBuilder.getMailLetter();

    @BeforeClass(description = "Login to account Yandex")
    public void loginToAccount() {
        loginGuiService.loginToAccountMailbox(userAccount);
    }

    @BeforeClass(description = "Send Mail")
    public void sendMail() {
        mailGuiService.sendMail(letter);
    }

    @Test(description = "Mark Mail as Spam")
    public void markMailAsSpam() {
        mailGuiService.markMailAsSpam(letter);
    }

    @Test(description = "Mark Mail as not Spam", dependsOnMethods = "markMailAsSpam")
    public void markMailAsNotSpam() {
        mailGuiService.markMailAsNotSpam(letter);
    }

    @Test(description = "Check mail in Inbox list", dependsOnMethods = "markMailAsNotSpam")
    public void checkMailInInboxList() {
        mailGuiService.checkMailInInbox(letter);
    }

    @Test(description = "Check mail disappear in Spam list", dependsOnMethods = "checkMailInInboxList")
    public void checkMailDisappearInSpamList() {
        mailGuiService.checkMailDisappearInSpam(letter);
    }
}