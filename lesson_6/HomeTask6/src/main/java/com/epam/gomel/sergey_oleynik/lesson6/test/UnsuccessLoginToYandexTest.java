package com.epam.gomel.sergey_oleynik.lesson6.test;

import com.epam.gomel.sergey_oleynik.lesson6.bo.common.Account;
import com.epam.gomel.sergey_oleynik.lesson6.bo.common.AccountBuilder;
import com.epam.gomel.sergey_oleynik.lesson6.pages.MailBasePage;
import com.epam.gomel.sergey_oleynik.lesson6.pages.MailLoginPage;
import com.epam.gomel.sergey_oleynik.lesson6.service.LoginGuiService;
import com.epam.gomel.sergey_oleynik.lesson6.ui.Browser;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;

public class UnsuccessLoginToYandexTest {

    public class LoginToYandexTest {
        private LoginGuiService loginGuiService = new LoginGuiService();
        private Account userAccount = AccountBuilder.getAccountIncorrectPassword();

        @Test(description = "Unsuccess login to Yandex")
        public void unsuccessLoginToAccount() {
            loginGuiService.loginToAccountError(userAccount);
        }

        @Test(description = "Check Unsuccess Login", dependsOnMethods = "unsuccessLoginToAccount")
        public void checkUnsuccessLogin() {
            loginGuiService.checkUnsuccessLogin();
        }
    }
}