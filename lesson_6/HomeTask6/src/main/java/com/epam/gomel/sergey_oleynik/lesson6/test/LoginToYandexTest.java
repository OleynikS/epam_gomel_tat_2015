package com.epam.gomel.sergey_oleynik.lesson6.test;

import com.epam.gomel.sergey_oleynik.lesson6.bo.common.Account;
import com.epam.gomel.sergey_oleynik.lesson6.bo.common.AccountBuilder;
import com.epam.gomel.sergey_oleynik.lesson6.service.LoginGuiService;
import org.testng.annotations.Test;

public class LoginToYandexTest {
    private LoginGuiService loginGuiService = new LoginGuiService();
    private Account userAccount = AccountBuilder.getAccount();

    @Test(description = "Login to account Yandex")
    public void loginToAccount() {
        loginGuiService.loginToAccountMailbox(userAccount);
    }

    @Test(description = "Check Success Login", dependsOnMethods = "loginToAccount")
    public void checkSuccessLogin() {
        loginGuiService.checkSuccessLogin(userAccount);
    }
}