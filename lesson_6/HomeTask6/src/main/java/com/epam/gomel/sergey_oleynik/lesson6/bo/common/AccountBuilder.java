package com.epam.gomel.sergey_oleynik.lesson6.bo.common;

import com.epam.gomel.sergey_oleynik.lesson6.utils.MyRandomUtils;

public class AccountBuilder {

    private static String userLogin = "OSV.test";
    private static String userPassword = "OSV_test";
    private static String userEmail = "OSV.test@yandex.ru";
    private static final int LENGTH_RANDOM_INCORRECT_PASSWORD = 10;
    private static String userIncorrectPassword = MyRandomUtils.randomString(LENGTH_RANDOM_INCORRECT_PASSWORD);

    public static Account getAccount() {
        Account account = new Account (userLogin, userPassword, userEmail);
        return account;
    }

    public static Account getAccountIncorrectPassword() {
        Account account = new Account (userLogin, userIncorrectPassword, userEmail);
        return account;
    }
}