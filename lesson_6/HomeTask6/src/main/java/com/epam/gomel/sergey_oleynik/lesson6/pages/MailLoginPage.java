package com.epam.gomel.sergey_oleynik.lesson6.pages;

import com.epam.gomel.sergey_oleynik.lesson6.reporting.Logger;
import com.epam.gomel.sergey_oleynik.lesson6.service.DataService;

public class MailLoginPage extends AbstractPage {

    public MailLoginPage open() {
        browser.open(DataService.getBaseUrl());
        Logger.log.info("Open url: " + DataService.getBaseUrl());
        return this;
    }

    public MailBasePage login(String login, String password) {
        browser.type(DataService.getLoginInputLocator(), login);
        Logger.log.info("Input login: " + login);
        browser.type(DataService.getPasswordInputLocator(), password);
        Logger.log.info("Input password: " + password);
        browser.click(DataService.getLoginButtonLocator());
        Logger.log.info("Click button 'Войти'");
        return new MailBasePage();
    }
}