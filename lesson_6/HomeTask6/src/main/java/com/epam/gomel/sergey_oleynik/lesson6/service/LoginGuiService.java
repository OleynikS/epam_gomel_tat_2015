package com.epam.gomel.sergey_oleynik.lesson6.service;

import com.epam.gomel.sergey_oleynik.lesson6.bo.common.Account;
import com.epam.gomel.sergey_oleynik.lesson6.exception.TestCommonRuntimeException;
import com.epam.gomel.sergey_oleynik.lesson6.pages.MailBasePage;
import com.epam.gomel.sergey_oleynik.lesson6.pages.MailLoginPage;
import com.epam.gomel.sergey_oleynik.lesson6.reporting.Logger;
import org.testng.Assert;

public class LoginGuiService {

    public void loginToAccountMailbox(Account account) {
        Logger.log.info("Login to account " + account.getEmail());
        MailBasePage mailbox = new MailLoginPage().open().login(account.getLogin(), account.getPassword());
        String nameAccount = mailbox.getNameAccount();
        if (nameAccount == null || !nameAccount.equals(account.getEmail())) {
            Logger.log.info("Login '" + account.getLogin() + "' or password '" + account.getPassword() + " is incorrect!");
            throw new TestCommonRuntimeException("Login failed");
        }
    }

    public void loginToAccountError(Account account) {
        Logger.log.info("Login to account " + account.getLogin() + " with incorrect password");
        MailBasePage mailbox = new MailLoginPage().open().login(account.getLogin(), account.getPassword());
        String errorMessage = mailbox.getErrorText();
        if (errorMessage == null || !errorMessage.equals(DataService.getErrorMessage())) {
            throw new TestCommonRuntimeException("Error message is incorrect");
        }
    }

    public void checkSuccessLogin(Account account) {
        Assert.assertTrue(new VerifyService().isAccountPresent(account.getEmail()));
        Logger.log.info("Success login to account " + account.getLogin());
    }

    public void checkUnsuccessLogin() {
        Assert.assertTrue(new VerifyService().isErrorPresent());
        Logger.log.info("Unsuccess login to account");
    }
}