package com.epam.gomel.sergey_oleynik.lesson6.service;

import org.openqa.selenium.By;

public class DataService {

    private static final By INBOX_LINK_LOCATOR = By.xpath("//a[@href='#inbox']");
    private static final String INBOX_MAIL_SUBJECT_LINK_LOCATOR_PATTERN = "//div[@class='block-messages' and @style!='display: none;']//span[@title='%s']";
    private static final String INBOX_MAIL_CHECKBOX_LOCATOR_PATTERN = "//div[@class='block-messages' and @style!='display: none;']//span[@title='%s']/ancestor::div[contains(@class,'b-messages__message')]//input";
    private static final By COMPOSE_BUTTON_LOCATOR = By.xpath("//a[@href='#compose']");
    private static final By TO_INPUT_LOCATOR = By.xpath("//tr[contains(@class, 'field_to')]//input[@type='text']");
    private static final By SUBJECT_INPUT_LOCATOR = By.id("compose-subj");
    private static final By MAIL_TEXT_LOCATOR = By.id("compose-send");
    private static final By SEND_MAIL_BUTTON_LOCATOR = By.id("compose-submit");
    private static final By ATTACH_FILE_BUTTON_LOCATOR = By.xpath("//input[@name='att']");
    private static final By DOWNLOAD_FILE_LINK_LOCATOR = By.xpath("//div[@class='js-attachments-icons-view']//a[@data-params='metrika=attach:body:loadfile']");
    private static final By DELETE_MAIL_BUTTON_LOCATOR = By.xpath("//a[@title='Удалить (Delete)']");
    private static final By SPAM_BUTTON_LOCATOR = By.xpath("//a[@title='Это спам! (Shift + s)']");

    private static final By SENT_LINK_LOCATOR = By.xpath("//a[@href='#sent']");
    private static final String SENT_MAIL_SUBJECT_LINK_LOCATOR_PATTERN = "//div[@class='block-messages']//span[@title='%s']";

    private static final By TRASH_LINK_LOCATOR = By.xpath("//a[@href='#trash']");
    private static final String TRASH_MAIL_SUBJECT_LINK_LOCATOR_PATTERN = "//div[@class='block-messages' and not(@style)]//span[@title='%s']";

    private static final By SPAM_LINK_LOCATOR = By.xpath("//a[@href='#spam']");
    private static final String SPAM_MAIL_SUBJECT_LINK_LOCATOR_PATTERN = "//div[@class='block-messages' and not(@style)]//span[@title='%s']";
    private static final String SPAM_MAIL_CHECKBOX_LOCATOR_PATTERN = "//div[@class='block-messages' and not(@style)]//span[@title='%s']/ancestor::div[contains(@class,'b-messages__message')]//input";
    private static final By NOT_SPAM_BUTTON_LOCATOR = By.xpath("//a[@title='Не спам!']");

    private static final String BASE_URL = "http://www.mail.yandex.by";
    private static final By LOGIN_INPUT_LOCATOR = By.xpath("//input[@name='login']");
    private static final By PASSWORD_INPUT_LOCATOR = By.xpath("//input[@name='passwd']");
    private static final By LOGIN_BUTTON_LOCATOR = By.xpath("//span[@class='new-auth-submit new-auth-form-line__item']/button");
    private static final String ACCOUNT_LINK_LOCATOR_PATTERN = "//a[@id='nb-1']/span[contains(.,'%s')]";
    private static final By ACCOUNT_LINK_LOCATOR = By.xpath("//a[@id='nb-1']/span[1]");
    private static final By ERROR_LINK_LOCATOR = By.xpath("//div[@class='error-msg']");
    private static String errorMessage = "Неправильная пара логин-пароль! Авторизоваться не удалось.";
    private static final String PATH_FILE_ATTACH = "D:\\_EPAM\\HomeTasks\\epam_gomel_tat_2015\\lesson_6\\HomeTask6\\";
    private static final String DOWNLOAD_DIR = "D:\\_EPAM\\";

    public static String getErrorMessage() {
        return errorMessage;
    }

    public static String getSpamMailCheckboxLocatorPattern() {
        return SPAM_MAIL_CHECKBOX_LOCATOR_PATTERN;
    }

    public static By getNotSpamButtonLocator() {
        return NOT_SPAM_BUTTON_LOCATOR;
    }

    public static By getSpamButtonLocator() {
        return SPAM_BUTTON_LOCATOR;
    }

    public static By getDeleteMailButtonLocator() {
        return DELETE_MAIL_BUTTON_LOCATOR;
    }

    public static By getComposeButtonLocator() {
        return COMPOSE_BUTTON_LOCATOR;
    }

    public static String getInboxMailCheckboxLocatorPattern() {
        return INBOX_MAIL_CHECKBOX_LOCATOR_PATTERN;
    }

    public static String getBaseUrl() {
        return BASE_URL;
    }

    public static By getLoginInputLocator() {
        return LOGIN_INPUT_LOCATOR;
    }

    public static By getPasswordInputLocator() {
        return PASSWORD_INPUT_LOCATOR;
    }

    public static By getLoginButtonLocator() {
        return LOGIN_BUTTON_LOCATOR;
    }

    public static By getDownloadFileLinkLocator() {
        return DOWNLOAD_FILE_LINK_LOCATOR;
    }

    public static By getToInputLocator() {
        return TO_INPUT_LOCATOR;
    }

    public static By getSubjectInputLocator() {
        return SUBJECT_INPUT_LOCATOR;
    }

    public static By getMailTextLocator() {
        return MAIL_TEXT_LOCATOR;
    }

    public static By getSendMailButtonLocator() {
        return SEND_MAIL_BUTTON_LOCATOR;
    }

    public static By getAttachFileButtonLocator() {
        return ATTACH_FILE_BUTTON_LOCATOR;
    }

    public static By getInboxLinkLocator() {
        return INBOX_LINK_LOCATOR;
    }

    public static By getSentLinkLocator() {
        return SENT_LINK_LOCATOR;
    }

    public static By getTrashLinkLocator() {
        return TRASH_LINK_LOCATOR;
    }

    public static By getSpamLinkLocator() {
        return SPAM_LINK_LOCATOR;
    }

    public static By getAccountLinkLocator() {
        return ACCOUNT_LINK_LOCATOR;
    }

    public static String getPathFileAttach() {
        return PATH_FILE_ATTACH;
    }

    public static String getDownloadDir() {
        return DOWNLOAD_DIR;
    }

    public static String getInboxMailSubjectLinkLocatorPattern() {
        return INBOX_MAIL_SUBJECT_LINK_LOCATOR_PATTERN;
    }

    public static String getSentMailSubjectLinkLocatorPattern() {
        return SENT_MAIL_SUBJECT_LINK_LOCATOR_PATTERN;
    }

    public static String getTrashMailSubjectLinkLocatorPattern() {
        return TRASH_MAIL_SUBJECT_LINK_LOCATOR_PATTERN;
    }

    public static String getSpamMailSubjectLinkLocatorPattern() {
        return SPAM_MAIL_SUBJECT_LINK_LOCATOR_PATTERN;
    }

    public static String getAccountLinkLocatorPattern() {
        return ACCOUNT_LINK_LOCATOR_PATTERN;
    }

    public static By getErrorLinkLocator() {
        return ERROR_LINK_LOCATOR;
    }
}