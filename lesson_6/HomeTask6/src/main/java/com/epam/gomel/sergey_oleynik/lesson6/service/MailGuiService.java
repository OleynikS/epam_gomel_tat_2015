package com.epam.gomel.sergey_oleynik.lesson6.service;

import com.epam.gomel.sergey_oleynik.lesson6.bo.mail.MailLetter;
import com.epam.gomel.sergey_oleynik.lesson6.pages.LetterContentPage;
import com.epam.gomel.sergey_oleynik.lesson6.pages.MailBasePage;
import com.epam.gomel.sergey_oleynik.lesson6.pages.MailSentListPage;
import com.epam.gomel.sergey_oleynik.lesson6.reporting.Logger;
import com.epam.gomel.sergey_oleynik.lesson6.utils.MyFileUtils;
import com.epam.gomel.sergey_oleynik.lesson6.utils.MyTimeOut;
import org.apache.commons.io.FileUtils;
import org.testng.Assert;

public class MailGuiService {

    public void sendMail(MailLetter letter) {
        MailBasePage mailbox = new MailBasePage().open();
        if(letter.getAttach() != null) {
            mailbox.openInboxPage().openComposeMailPage().sendMail(letter.getReceiver(), letter.getSubject(), letter.getContent(), letter.getAttach());
//            Logger.log.info("Send mail to " + letter.getReceiver() + " with attached file '" + letter.getAttach() + "' , Subject: '" + letter.getSubject() + "' , Content: '" + letter.getContent() + "'");
        }
        else {
            mailbox.openInboxPage().openComposeMailPage().sendMail(letter.getReceiver(), letter.getSubject(), letter.getContent());
//            Logger.log.info("Send mail to " + letter.getReceiver() + " , Subject: '" + letter.getSubject() + "' , Content: '" + letter.getContent() + "'");
        }
    }

    public void deleteMail(MailLetter letter) {
        new MailBasePage().openInboxPage().deleteInboxMail(letter.getSubject());
        Logger.log.info("Deleted Mail with subject: '" + letter.getSubject() + "'");
    }

    public void markMailAsSpam(MailLetter letter) {
        new MailBasePage().openInboxPage().markSpam(letter.getSubject());
        Logger.log.info("Mail with subject: '" + letter.getSubject() + "' mark as spam");
    }

    public void markMailAsNotSpam(MailLetter letter) {
        new MailBasePage().openSpamPage().markNotSpam(letter.getSubject());
        Logger.log.info("Mail with subject: '" + letter.getSubject() + "' mark as not spam");
    }

    public void checkMailInInbox(MailLetter letter) {
        new MailBasePage().openInboxPage();
        Assert.assertTrue(new VerifyService().isMessagePresentInboxList(letter.getSubject()));
        Logger.log.info("Mail with subject '" + letter.getSubject() + "' in Inbox list");
    }

    public void checkMailInSent(MailLetter letter) {
        new MailBasePage().openSentPage();
        Assert.assertTrue(new VerifyService().isMessagePresentSentList(letter.getSubject()));
        Logger.log.info("Mail with subject '" + letter.getSubject() + "' in Sent list");
    }

    public void checkMailInTrash(MailLetter letter) {
        new MailBasePage().openTrashPage();
        Assert.assertTrue(new VerifyService().isMessagePresentTrashList(letter.getSubject()));
        Logger.log.info("Mail with subject '" + letter.getSubject() + "' in Trash list");
    }

    public void checkMailInSpam(MailLetter letter) {
        new MailBasePage().openSpamPage();
        Assert.assertTrue(new VerifyService().isMessagePresentSpamList(letter.getSubject()));
        Logger.log.info("Mail with subject '" + letter.getSubject() + "' in Spam list");
    }

    public void checkMailDisappearInSpam(MailLetter letter) {
        new MailBasePage().openSpamPage();
        Assert.assertFalse(new VerifyService().isMessageNotPresentSpamList(letter.getSubject()));
        Logger.log.info("Mail with subject '" + letter.getSubject() + "' disappear in Spam list");
    }

    public void checkContentFile(MailLetter letter) {
        new MailSentListPage().openLetter(letter.getSubject());
        new LetterContentPage().downloadAttachFile();
        MyTimeOut.timeOut();
        try {
            FileUtils.contentEquals(MyFileUtils.createAttachFile(), MyFileUtils.getDownloadFile());
            Logger.log.info("Content of files '" + MyFileUtils.createAttachFile() + "' and '" + MyFileUtils.getDownloadFile() + "' equals");
        } catch (Exception e) {
            Logger.log.info("Content of files '" + MyFileUtils.createAttachFile() + "' and '" + MyFileUtils.getDownloadFile() + "' not equals");
        }
    }
}