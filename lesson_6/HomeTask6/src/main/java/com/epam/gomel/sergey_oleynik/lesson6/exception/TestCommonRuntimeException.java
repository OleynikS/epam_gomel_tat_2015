package com.epam.gomel.sergey_oleynik.lesson6.exception;

public class TestCommonRuntimeException extends RuntimeException {

    public TestCommonRuntimeException(String message) {
        super(message);
    }

    public TestCommonRuntimeException(String message, Throwable cause) {
        super(message, cause);
    }
}