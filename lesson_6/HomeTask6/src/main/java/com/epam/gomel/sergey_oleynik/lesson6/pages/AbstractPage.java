package com.epam.gomel.sergey_oleynik.lesson6.pages;

import com.epam.gomel.sergey_oleynik.lesson6.ui.Browser;

public abstract class AbstractPage {

    protected Browser browser;

    public AbstractPage() {
        this.browser = Browser.get();
    }
}
