package com.epam.gomel.sergey_oleynik.lesson6.test;

import com.epam.gomel.sergey_oleynik.lesson6.bo.common.Account;
import com.epam.gomel.sergey_oleynik.lesson6.bo.common.AccountBuilder;
import com.epam.gomel.sergey_oleynik.lesson6.bo.mail.MailLetter;
import com.epam.gomel.sergey_oleynik.lesson6.bo.mail.MailLetterBuilder;
import com.epam.gomel.sergey_oleynik.lesson6.service.LoginGuiService;
import com.epam.gomel.sergey_oleynik.lesson6.service.MailGuiService;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class SendEmailWithAttachTest {
    private LoginGuiService loginGuiService = new LoginGuiService();
    private Account userAccount = AccountBuilder.getAccount();
    private MailGuiService mailGuiService = new MailGuiService();
    private MailLetter letter = MailLetterBuilder.getMailLetterWithAtach();

    @BeforeClass(description = "Login to account Yandex")
    public void loginToAccount() {
        loginGuiService.loginToAccountMailbox(userAccount);
    }

    @Test(description = "Send Mail with Attach")
    public void sendMailWithAttach() {
        mailGuiService.sendMail(letter);
    }

    @Test(description = "Check mail in Sent list", dependsOnMethods = "sendMailWithAttach")
    public void checkMailInSentList() {
        mailGuiService.checkMailInSent(letter);
    }

    @Test(description = "Check Content Download File", dependsOnMethods = "checkMailInSentList")
    public void checkContentDownloadFile() {
        mailGuiService.checkContentFile(letter);
    }
}