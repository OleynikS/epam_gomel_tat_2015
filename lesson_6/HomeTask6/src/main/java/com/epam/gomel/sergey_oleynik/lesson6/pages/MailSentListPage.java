package com.epam.gomel.sergey_oleynik.lesson6.pages;

import com.epam.gomel.sergey_oleynik.lesson6.reporting.Logger;
import com.epam.gomel.sergey_oleynik.lesson6.service.DataService;

public class MailSentListPage extends AbstractPage {

    public LetterContentPage openLetter(String subject) {
        browser.clickPattern(DataService.getSentMailSubjectLinkLocatorPattern(), subject);
        Logger.log.info("Open letter with Subject: " + subject);
        return new LetterContentPage();
    }
}