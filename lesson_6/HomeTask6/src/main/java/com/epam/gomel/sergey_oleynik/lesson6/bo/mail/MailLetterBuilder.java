package com.epam.gomel.sergey_oleynik.lesson6.bo.mail;

import com.epam.gomel.sergey_oleynik.lesson6.service.DataService;
import com.epam.gomel.sergey_oleynik.lesson6.utils.MyFileUtils;
import com.epam.gomel.sergey_oleynik.lesson6.utils.MyRandomUtils;

public class MailLetterBuilder {
    private static final int LENGTH_MAIL_RANDOM_SUBJECT = 10;
    private static final int LENGTH_MAIL_RANDOM_CONTENT = 50;

    private static String mailTo = "OSV.test@yandex.ru";
    private static String mailSubject = "Test subject " + MyRandomUtils.randomString(LENGTH_MAIL_RANDOM_SUBJECT);
    private static String mailContent = "Mail content " + MyRandomUtils.randomString(LENGTH_MAIL_RANDOM_CONTENT);

    public static MailLetter getMailLetter() {
         return new MailLetter(mailTo, mailSubject, mailContent);
    }

    public static MailLetter getMailLetterWithAtach() {
         return new MailLetter(mailTo, mailSubject, mailContent, DataService.getPathFileAttach() + MyFileUtils.createAttachFile().getName());
    }
}