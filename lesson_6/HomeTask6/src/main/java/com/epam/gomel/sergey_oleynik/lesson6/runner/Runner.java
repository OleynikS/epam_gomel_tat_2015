package com.epam.gomel.sergey_oleynik.lesson6.runner;

import com.epam.gomel.sergey_oleynik.lesson6.reporting.CustomTestNgListener;
import org.testng.TestNG;
import java.util.ArrayList;
import java.util.List;

public class Runner {

    public static void main(String[] args) {
        TestNG testNG = new TestNG();
        List<String> suites = new ArrayList<String>();
        testNG.addListener(new CustomTestNgListener());
        suites.add("target/classes/suites/login_suit.xml");
//        suites.add("target/classes/suites/send_mail_suit.xml");
        testNG.setTestSuites(suites);
        testNG.run();
    }
}