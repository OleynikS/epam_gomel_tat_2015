package com.epam.gomel.sergey_oleynik.lesson6.utils;

public class MyTimeOut {
    private static final int TIMEOUT = 2500;

    public static void timeOut() {
        try {
            Thread.sleep(TIMEOUT);
        } catch (Exception e) { }
    }
}