package com.epam.gomel.sergey_oleynik.lesson6.pages;

import com.epam.gomel.sergey_oleynik.lesson6.reporting.Logger;
import com.epam.gomel.sergey_oleynik.lesson6.service.DataService;

public class MailSpamListPage extends AbstractPage {

    public MailBasePage markNotSpam (String subject) {
        browser.clickPattern(DataService.getSpamMailCheckboxLocatorPattern(), subject);
        Logger.log.info("Check mail with subject '" + subject + "'");
        browser.click(DataService.getNotSpamButtonLocator());
        Logger.log.info("Click button 'Не спам!'");
        return new MailBasePage();
    }
}