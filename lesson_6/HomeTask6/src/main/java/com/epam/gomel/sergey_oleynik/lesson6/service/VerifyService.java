package com.epam.gomel.sergey_oleynik.lesson6.service;

import com.epam.gomel.sergey_oleynik.lesson6.pages.AbstractPage;
import org.openqa.selenium.By;

public class VerifyService extends AbstractPage {

    public boolean isMessagePresentInboxList(String subject) {
        try {
            browser.waitForVisible(By.xpath(String.format(DataService.getInboxMailSubjectLinkLocatorPattern(), subject)));
            return true;
        } catch (Exception e) {return false;}
    }

    public boolean isMessagePresentSentList(String subject) {
        try {
            browser.waitForVisible(By.xpath(String.format(DataService.getSentMailSubjectLinkLocatorPattern(), subject)));
            return true;
        } catch (Exception e) {return false;}
    }

    public boolean isMessagePresentTrashList(String subject) {
        try {
            browser.waitForVisible(By.xpath(String.format(DataService.getTrashMailSubjectLinkLocatorPattern(), subject)));
            return true;
        } catch (Exception e) {return false;}
    }

    public boolean isMessagePresentSpamList(String subject) {
        try {
            browser.waitForVisible(By.xpath(String.format(DataService.getSpamMailSubjectLinkLocatorPattern(), subject)));
            return true;
        } catch (Exception e) {return false;}
    }

    public boolean isMessageNotPresentSpamList(String subject) {
//        browser.waitForAjaxProcessed();
        return browser.isPresent(By.xpath(String.format(DataService.getSpamMailSubjectLinkLocatorPattern(), subject)));
    }

    public boolean isAccountPresent(String account) {
        try {
            browser.waitForVisible(By.xpath(String.format(DataService.getAccountLinkLocatorPattern(), account)));
            return true;
        } catch (Exception e) {return false;}
    }

    public boolean isErrorPresent() {
        try {
            browser.waitForVisible(DataService.getErrorLinkLocator());
            return true;
        } catch (Exception e) {return false;}
    }
}