package com.epam.gomel.sergey_oleynik.lesson6.reporting;

public class Logger {

    public static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(Logger.class);

    public static void trace(String s) {
        log.trace(s);
    }

    public static void trace(String s, Throwable e) {
        log.trace(s, e);
    }

    public static void debug(String s) {
        log.debug(s);
    }

    public static void debug(String s, Throwable e) {
        log.debug(s, e);
    }

    public static void info(String s) {
        log.info(s);
    }

    public static void info(String s, Throwable e) {
        log.info(s, e);
    }

    public static void warn(String s) {
        log.warn(s);
    }

    public static void warn(String s, Throwable e) {
        log.warn(s, e);
    }

    public static void error(String s) {
        log.error(s);
    }

    public static void error(String s, Throwable e) {
        log.error(s, e);
    }

    public static void fatal(String s) {
        log.fatal(s);
    }

    public static void fatal(String s, Throwable e) {
        log.fatal(s, e);
    }

}