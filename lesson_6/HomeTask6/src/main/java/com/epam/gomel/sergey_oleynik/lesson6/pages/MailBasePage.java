package com.epam.gomel.sergey_oleynik.lesson6.pages;

import com.epam.gomel.sergey_oleynik.lesson6.reporting.Logger;
import com.epam.gomel.sergey_oleynik.lesson6.service.DataService;

public class MailBasePage extends AbstractPage {

    public MailBasePage open() {
        browser.open(DataService.getBaseUrl());
        return new MailBasePage();
    }

    public MailInboxListPage openInboxPage() {
        browser.click(DataService.getInboxLinkLocator());
        Logger.log.info("Open Inbox list");
        return new MailInboxListPage();
    }

    public MailSentListPage openSentPage() {
        browser.click(DataService.getSentLinkLocator());
        Logger.log.info("Open Sent list");
        return new MailSentListPage();
    }

    public MailTrashListPage openTrashPage() {
        browser.click(DataService.getTrashLinkLocator());
        Logger.log.info("Open Trash list");
        return new MailTrashListPage();
    }

    public MailSpamListPage openSpamPage() {
        browser.click(DataService.getSpamLinkLocator());
        Logger.log.info("Open Spam list");
        return new MailSpamListPage();
    }

    public String getNameAccount() {
        try {
            return browser.elementText(DataService.getAccountLinkLocator());
        } catch (Exception e) {return null;}
    }

    public String getErrorText() {
        try {
            return browser.elementText(DataService.getErrorLinkLocator());
        } catch (Exception e) {return null;}
    }
}