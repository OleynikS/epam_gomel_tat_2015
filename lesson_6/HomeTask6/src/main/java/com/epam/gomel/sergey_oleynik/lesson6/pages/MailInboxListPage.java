package com.epam.gomel.sergey_oleynik.lesson6.pages;

import com.epam.gomel.sergey_oleynik.lesson6.reporting.Logger;
import com.epam.gomel.sergey_oleynik.lesson6.service.DataService;

public class MailInboxListPage extends AbstractPage {

    public ComposeMailPage openComposeMailPage() {
        browser.click(DataService.getComposeButtonLocator());
        Logger.log.info("Click button 'Написать'" );
        return new ComposeMailPage();
    }

    public MailBasePage deleteInboxMail(String subject) {
        browser.clickPattern(DataService.getInboxMailCheckboxLocatorPattern(), subject);
        Logger.log.info("Check mail with subject '" + subject + "'");
        browser.click(DataService.getDeleteMailButtonLocator());
        Logger.log.info("Click button 'Удалить'");
        return new MailBasePage();
    }

    public MailBasePage markSpam (String subject) {
        browser.clickPattern(DataService.getInboxMailCheckboxLocatorPattern(), subject);
        Logger.log.info("Check mail with subject '" + subject + "'");
        browser.click(DataService.getSpamButtonLocator());
        Logger.log.info("Click button 'Это спам!'");
        return new MailBasePage();
    }
}