package com.epam.gomel.sergey_oleynik.lesson7.utils;

import com.epam.gomel.sergey_oleynik.lesson7.reporting.Logger;
import com.epam.gomel.sergey_oleynik.lesson7.service.DataService;
import org.apache.commons.io.FileUtils;
import java.io.File;
import java.io.IOException;
import java.net.URL;

public class MyFileUtils {

    private static final int LENGTH_FILE_RANDOM_NAME = 10;
    private static final int LENGTH_FILE_RANDOM_CONTENT = 100;
    private static String randomNameFile = MyRandomUtils.randomString(LENGTH_FILE_RANDOM_NAME) + ".txt";

    public static File createAttachFile() {
        File fileAttach = new File(DataService.getPathFileAttach() + randomNameFile);
        try {
            boolean exists = fileAttach.createNewFile();
        if (exists) {
            FileUtils.write(fileAttach, "File content "+MyRandomUtils.randomString(LENGTH_FILE_RANDOM_CONTENT));
        }
        } catch (Exception e) {
            Logger.log.error("File not created");
        }
        return fileAttach;
    }

    public static File getDownloadFile() {
        File fileDownload = new File(DataService.getDownloadDir() + randomNameFile);
        return fileDownload;
    }

    public static String getCanonicalPathToResourceFile(String resourceFileLocalPath) {
        try {
            URL url = MyFileUtils.class.getResource(resourceFileLocalPath);
            File file = new File(url.getPath());
            return file.getCanonicalPath();
        } catch (IOException e) {
            Logger.log.error(e.getMessage(), e);
        }
        return null;
    }
}