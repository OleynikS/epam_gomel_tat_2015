package com.epam.gomel.sergey_oleynik.lesson7.runner;

import com.epam.gomel.sergey_oleynik.lesson7.GlobalConfig;
import com.epam.gomel.sergey_oleynik.lesson7.cli.TestRunnerOptions;
import com.epam.gomel.sergey_oleynik.lesson7.utils.MyFileUtils;
import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;
import org.testng.TestListenerAdapter;
import org.testng.TestNG;
import org.testng.xml.XmlSuite;

import java.util.ArrayList;
import java.util.List;

public class Runner {

    public static void main(String[] args) {
        new Runner(args).runTests();
    }

    private Runner(String[] args) {
        parseCli(args);
    }

    private void parseCli(String[] args) {
        TestRunnerOptions options = new TestRunnerOptions();
        CmdLineParser parser = new CmdLineParser(options);
        try {
            parser.parseArgument(args);
            GlobalConfig.updateFromOptions(options);
            System.out.println();
        } catch( CmdLineException e ) {
            System.err.println(e.getMessage());
            parser.printUsage(System.err);
            System.err.println();
            return;
        }
        System.out.println(options);
    }

    private void runTests() {
        TestListenerAdapter tla = new TestListenerAdapter();
        TestNG tng = new TestNG();
        tng.addListener(tla);
        XmlSuite suite = new XmlSuite();
        suite.setName("TmpSuite");

        List<String> files = new ArrayList<>();
        files.add(MyFileUtils.getCanonicalPathToResourceFile("/suites/login_suit.xml"));
//        files.add(MyFileUtils.getCanonicalPathToResourceFile("/suites/send_mail_suit.xml"));
        suite.setSuiteFiles(files);

        List<XmlSuite> suites = new ArrayList<XmlSuite>();
        suites.add(suite);
        tng.setXmlSuites(suites);

        tng.setParallel(GlobalConfig.getParallelMode().getAlias());
        tng.setThreadCount(GlobalConfig.getThreadCount());
        tng.run();
    }
}