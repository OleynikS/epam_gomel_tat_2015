package com.epam.gomel.sergey_oleynik.lesson7.ui;

public enum BrowserType {
    FIREFOX("firefox"),
    CHROME("chrome");

    private String alias;

    BrowserType(String alias) {
        this.alias = alias;
    }

    public static BrowserType getTypeByAlias(String alias) {
        for(BrowserType type: BrowserType.values()){
            if(type.getAlias().equals(alias.toLowerCase())){
                return type;
            }
        }
        throw new RuntimeException("No such enum value");
    }

    public String getAlias() {
        return alias;
    }
}
