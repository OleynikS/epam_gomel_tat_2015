package com.epam.gomel.sergey_oleynik.lesson7.pages;

import com.epam.gomel.sergey_oleynik.lesson7.ui.Browser;

public abstract class AbstractPage {

    protected Browser browser;

    public AbstractPage() {
        this.browser = Browser.get();
    }
}
