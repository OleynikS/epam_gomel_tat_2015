package com.epam.gomel.sergey_oleynik.lesson7.pages;

import com.epam.gomel.sergey_oleynik.lesson7.reporting.Logger;
import com.epam.gomel.sergey_oleynik.lesson7.service.DataService;

public class ComposeMailPage extends AbstractPage {

    public MailBasePage sendMail(String mailTo, String mailSubject, String mailContent) {
        browser.type(DataService.getToInputLocator(), mailTo);
        Logger.log.info("Input mail to: " + mailTo);
        browser.type(DataService.getSubjectInputLocator(), mailSubject);
        Logger.log.info("Input mail subject: " + mailSubject);
        browser.type(DataService.getMailTextLocator(), mailContent);
        Logger.log.info("Input mail content: " + mailContent);
        browser.click(DataService.getSendMailButtonLocator());
        Logger.log.info("Click button 'Отправить'");
        browser.waitForAjaxProcessed();
        return new MailBasePage();
    }

    public MailBasePage sendMail(String mailTo, String mailSubject, String mailContent, String attach) {
        browser.type(DataService.getToInputLocator(), mailTo);
        Logger.log.info("Input mail to: " + mailTo);
        browser.type(DataService.getSubjectInputLocator(), mailSubject);
        Logger.log.info("Input mail subject: " + mailSubject);
        browser.type(DataService.getMailTextLocator(), mailContent);
        Logger.log.info("Input mail content: " + mailContent);
        if(attach != null) {
            browser.attachFile(DataService.getAttachFileButtonLocator(), attach);
            Logger.log.info("File attached '" + attach + "'");
        }
        browser.click(DataService.getSendMailButtonLocator());
        Logger.log.info("Click button 'Отправить'");
        browser.waitForAjaxProcessed();
        return new MailBasePage();
    }
}