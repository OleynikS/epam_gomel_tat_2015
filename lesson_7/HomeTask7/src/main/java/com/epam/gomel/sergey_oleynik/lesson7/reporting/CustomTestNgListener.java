package com.epam.gomel.sergey_oleynik.lesson7.reporting;

import com.epam.gomel.sergey_oleynik.lesson7.ui.Browser;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.internal.IResultListener2;

public class CustomTestNgListener implements IResultListener2 {

    @Override
    public void onStart(ITestContext context) {
        System.out.println("START : " + context.getCurrentXmlTest().getName());
    }

    @Override
    public void onFinish(ITestContext context) {
        Browser.get().close();
        System.out.println("FINISH : " + context.getName());
    }

    @Override
    public void onTestStart(ITestResult result) {
        System.out.println("TEST METHOD START : " + result.getMethod().getInstance().getClass().getCanonicalName() + "." + result.getMethod().getMethodName());
    }

    @Override
    public void onTestSuccess(ITestResult result) {
        System.out.println("TEST METHOD SUCCESS : " + result.getMethod().getInstance().getClass().getCanonicalName() + "." + result.getMethod().getMethodName());
    }

    @Override
    public void onTestFailure(ITestResult result) {
        Logger.log.info("TEST METHOD FAILED : " + result.getMethod().getInstance().getClass().getCanonicalName() + "." + result.getMethod().getMethodName());
        Logger.log.error("Test failed", result.getThrowable());
    }

    @Override
    public void onTestSkipped(ITestResult result) {
        System.out.println("TEST METHOD SKIPPED : " + result.getMethod().getInstance().getClass().getCanonicalName() + "." + result.getMethod().getMethodName());
    }

    @Override
    public void onTestFailedButWithinSuccessPercentage(ITestResult result) {
        System.out.println("TEST METHOD FAILED : " + result.getMethod().getInstance().getClass().getCanonicalName() + "." + result.getMethod().getMethodName());
        System.err.println(result.getThrowable());
    }

    @Override
    public void beforeConfiguration(ITestResult result) {
        System.out.println("CONFIG START : " + result.getMethod().getInstance().getClass().getCanonicalName() + "." + result.getMethod().getMethodName());
    }

    @Override
    public void onConfigurationSuccess(ITestResult result) {
        System.out.println("CONFIG SUCCESS : " + result.getMethod().getInstance().getClass().getCanonicalName() + "." + result.getMethod().getMethodName());
    }

    @Override
    public void onConfigurationFailure(ITestResult result) {
        Logger.log.info("CONFIG FAILED : " + result.getMethod().getInstance().getClass().getCanonicalName() + "." + result.getMethod().getMethodName());
        Logger.log.error("Config failed", result.getThrowable());
    }

    @Override
    public void onConfigurationSkip(ITestResult result) {
        System.out.println("CONFIG SKIPPED : " + result.getMethod().getInstance().getClass().getCanonicalName() + "." + result.getMethod().getMethodName());
    }
}
