package com.epam.gomel.sergey_oleynik.lesson7.test;

import com.epam.gomel.sergey_oleynik.lesson7.bo.common.Account;
import com.epam.gomel.sergey_oleynik.lesson7.bo.common.AccountBuilder;
import com.epam.gomel.sergey_oleynik.lesson7.service.LoginGuiService;
import org.testng.annotations.Test;

public class UnsuccessLoginToYandexTest {

    public class LoginToYandexTest {
        private LoginGuiService loginGuiService = new LoginGuiService();
        private Account userAccount = AccountBuilder.getAccountIncorrectPassword();

        @Test(description = "Unsuccess login to Yandex")
        public void unsuccessLoginToAccount() {
            loginGuiService.loginToAccountError(userAccount);
        }

        @Test(description = "Check Unsuccess Login", dependsOnMethods = "unsuccessLoginToAccount")
        public void checkUnsuccessLogin() {
            loginGuiService.checkUnsuccessLogin();
        }
    }
}