package com.epam.gomel.sergey_oleynik.lesson7.test;

import com.epam.gomel.sergey_oleynik.lesson7.bo.common.Account;
import com.epam.gomel.sergey_oleynik.lesson7.bo.common.AccountBuilder;
import com.epam.gomel.sergey_oleynik.lesson7.bo.mail.MailLetter;
import com.epam.gomel.sergey_oleynik.lesson7.bo.mail.MailLetterBuilder;
import com.epam.gomel.sergey_oleynik.lesson7.service.LoginGuiService;
import com.epam.gomel.sergey_oleynik.lesson7.service.MailGuiService;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class DeleteEmailTest {
    private LoginGuiService loginGuiService = new LoginGuiService();
    private Account userAccount = AccountBuilder.getAccount();
    private MailGuiService mailGuiService = new MailGuiService();
    private MailLetter letter = MailLetterBuilder.getMailLetter();

    @BeforeClass(description = "Login to account Yandex")
    public void loginToAccount() {
        loginGuiService.loginToAccountMailbox(userAccount);
    }

    @BeforeClass(description = "Send Mail")
    public void sendMail() {
        mailGuiService.sendMail(letter);
    }

    @Test(description = "Delete Mail")
    public void deleteMail() {
        mailGuiService.deleteMail(letter);
    }

    @Test(description = "Check mail in Deleted list", dependsOnMethods = "deleteMail")
    public void checkMailInTrashList() {
        mailGuiService.checkMailInTrash(letter);
    }
}