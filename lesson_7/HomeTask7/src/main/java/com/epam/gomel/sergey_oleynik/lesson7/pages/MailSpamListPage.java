package com.epam.gomel.sergey_oleynik.lesson7.pages;

import com.epam.gomel.sergey_oleynik.lesson7.reporting.Logger;
import com.epam.gomel.sergey_oleynik.lesson7.service.DataService;

public class MailSpamListPage extends AbstractPage {

    public MailBasePage markNotSpam (String subject) {
        browser.clickPattern(DataService.getSpamMailCheckboxLocatorPattern(), subject);
        Logger.log.info("Check mail with subject '" + subject + "'");
        browser.click(DataService.getNotSpamButtonLocator());
        Logger.log.info("Click button 'Не спам!'");
        return new MailBasePage();
    }
}