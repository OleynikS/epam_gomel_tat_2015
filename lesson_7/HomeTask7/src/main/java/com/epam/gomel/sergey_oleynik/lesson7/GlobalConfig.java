package com.epam.gomel.sergey_oleynik.lesson7;

import com.epam.gomel.sergey_oleynik.lesson7.cli.TestRunnerOptions;
import com.epam.gomel.sergey_oleynik.lesson7.ui.BrowserType;

public class GlobalConfig {

    private static BrowserType browserType = BrowserType.FIREFOX;

    private static ParallelMode parallelMode = ParallelMode.FALSE;

    private static int threadCount = 1;

    public static void updateFromOptions(TestRunnerOptions options) {
        browserType = BrowserType.getTypeByAlias(options.browserType);
        parallelMode = ParallelMode.getTypeByAlias(options.parallelMode);
        threadCount = options.threadCount;
    }

    public static BrowserType getBrowserType() {
        return browserType;
    }

    public static void setBrowserType(BrowserType browserType) {
        GlobalConfig.browserType = browserType;
    }

    public static ParallelMode getParallelMode() {
        return parallelMode;
    }

    public static void setParallelMode(ParallelMode parallelMode) {
        GlobalConfig.parallelMode = parallelMode;
    }

    public static int getThreadCount() {
        return threadCount;
    }

    public static void setThreadCount(int threadCount) {
        GlobalConfig.threadCount = threadCount;
    }
}
