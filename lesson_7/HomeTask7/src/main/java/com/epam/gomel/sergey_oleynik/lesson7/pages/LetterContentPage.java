package com.epam.gomel.sergey_oleynik.lesson7.pages;

import com.epam.gomel.sergey_oleynik.lesson7.reporting.Logger;
import com.epam.gomel.sergey_oleynik.lesson7.service.DataService;
import com.epam.gomel.sergey_oleynik.lesson7.utils.MyFileUtils;

public class LetterContentPage extends AbstractPage {

    public void downloadAttachFile() {
        browser.click(DataService.getDownloadFileLinkLocator());
        Logger.log.info("Download file '" + MyFileUtils.getDownloadFile().getName() + "' to directory '" + DataService.getDownloadDir() + "'");
    }
}