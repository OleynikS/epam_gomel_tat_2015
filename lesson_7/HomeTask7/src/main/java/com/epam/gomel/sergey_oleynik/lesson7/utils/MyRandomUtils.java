package com.epam.gomel.sergey_oleynik.lesson7.utils;

import org.apache.commons.lang3.RandomStringUtils;

public class MyRandomUtils {

    public static String randomString(int length) {
        return RandomStringUtils.randomAlphabetic(length);
    }
}