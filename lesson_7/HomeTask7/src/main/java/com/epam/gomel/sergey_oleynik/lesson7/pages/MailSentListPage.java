package com.epam.gomel.sergey_oleynik.lesson7.pages;

import com.epam.gomel.sergey_oleynik.lesson7.reporting.Logger;
import com.epam.gomel.sergey_oleynik.lesson7.service.DataService;

public class MailSentListPage extends AbstractPage {

    public LetterContentPage openLetter(String subject) {
        browser.clickPattern(DataService.getSentMailSubjectLinkLocatorPattern(), subject);
        Logger.log.info("Open letter with Subject: " + subject);
        return new LetterContentPage();
    }
}