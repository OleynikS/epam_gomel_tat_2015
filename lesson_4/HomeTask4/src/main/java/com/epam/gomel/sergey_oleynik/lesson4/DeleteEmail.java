package com.epam.gomel.sergey_oleynik.lesson4;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import java.util.concurrent.TimeUnit;

public class DeleteEmail {

    // AUT data
    public static final String YA_URL = "http://www.yandex.by";

    // UI data
    public static final By ENTER_BUTTON_LOCATOR = By.xpath("//button[contains(@class, 'button user')]");
    public static final By LOGIN_INPUT_LOCATOR = By.id("b-mail-domik-username11");
    public static final By PASSWORD_INPUT_LOCATOR = By.id("b-mail-domik-password11");
    public static final By SUBMIT_BUTTON_LOCATOR = By.xpath("//input[@value='Войти']");
    public static final By COMPOSE_BUTTON_LOCATOR = By.xpath("//a[@href='#compose']");
    public static final By TO_INPUT_LOCATOR = By.xpath("//tr[contains(@class, 'field_to')]//input[@type='text']");
    public static final By SUBJECT_INPUT_LOCATOR = By.id("compose-subj");
    public static final By MAIL_TEXT_LOCATOR = By.id("compose-send");
    public static final By SEND_MAIL_BUTTON_LOCATOR = By.id("compose-submit");
    public static final By INBOX_LINK_LOCATOR = By.xpath("//a[@href='#inbox']");
    public static final String INBOX_CHECKBOX_LOCATOR_PATTERN = "//div[@class='block-messages' and @style!='display: none;']//span[@title='%s']/ancestor::div[contains(@class,'b-messages__message')]//input";
    public static final By DELETE_BUTTON_LOCATOR = By.xpath("//a[@title='Удалить (Delete)']");
    public static final By DELETEBOX_LINK_LOCATOR = By.xpath("//a[@href='#trash']");
    public static final String MAIL_LINK_LOCATOR_PATTERN = "//div[@class='block-messages' and not(@style)]//span[@title='%s']";
//    public static final String DELETEBOX_CHECKBOX_LOCATOR_PATTERN = "//div[@class='block-messages' and not(@style)]//span[@title='%s']/ancestor::div[contains(@class,'b-messages__message')]//input";
//    public static final By COUNT_MAIL_LOCATOR =By.xpath("//div[contains(@class, 'folder_trash')]//span[@class='b-folders__folder__counters__total']");

    // Tools data
    private WebDriver driver;
    public static final int DRIVER_PAGE_LOAD_TIMEOUT_SECONDS = 20;
    public static final int DRIVE_IMPL_WAIT_TIMEOUT_SECONDS = 5;
    public static final int TIME_OUT_MAIL_ARRIVED_SECONDS = 10;
    public static final int TIME_WAIT_SECONDS = 3000;

    // Test data
    private String userLogin = "OSV.test"; // Account Login
    private String userPassword = "OSV_test"; // Account password
    private String mailTo = "OSV.test@yandex.ru";
    private String mailSubject = "Test subject " + Math.random() * 1000000;
    private String mailContent = "Mail content " + Math.random() * 1000000;

    @BeforeClass(description = "Prepare browser")
    public void prepareBrowser() {
        driver = new FirefoxDriver();
        driver.manage().timeouts().pageLoadTimeout(DRIVER_PAGE_LOAD_TIMEOUT_SECONDS, TimeUnit.SECONDS);
        driver.manage().timeouts().implicitlyWait(DRIVE_IMPL_WAIT_TIMEOUT_SECONDS, TimeUnit.SECONDS);
    }

    @Test(description = "Delete Mail")
    public void deleteEMail() throws InterruptedException {
    //   Login
        driver.get(YA_URL);
        WebElement enterButton = driver.findElement(ENTER_BUTTON_LOCATOR);
        enterButton.click();
        WebElement loginInput = driver.findElement(LOGIN_INPUT_LOCATOR);
        loginInput.sendKeys(userLogin);
        WebElement passInput = driver.findElement(PASSWORD_INPUT_LOCATOR);
        passInput.sendKeys(userPassword);
        WebElement submitButton = driver.findElement(SUBMIT_BUTTON_LOCATOR);
        submitButton.click();

    //  Send Mail
        WebElement composeButton = driver.findElement(COMPOSE_BUTTON_LOCATOR);
        composeButton.click();
        WebElement toInput = driver.findElement(TO_INPUT_LOCATOR);
        toInput.sendKeys(mailTo);
        WebElement subjectInput = driver.findElement(SUBJECT_INPUT_LOCATOR);
        subjectInput.sendKeys(mailSubject);
        WebElement mailContentText = driver.findElement(MAIL_TEXT_LOCATOR);
        mailContentText.sendKeys(mailContent);
        WebElement sendMailButton = driver.findElement(SEND_MAIL_BUTTON_LOCATOR);
        sendMailButton.click();

    //  Delete Mail

       // new WebDriverWait(driver, TIME_OUT_MAIL_ARRIVED_SECONDS).until(ExpectedConditions.visibilityOfElementLocated(INBOX_LINK_LOCATOR));
        Thread.sleep(TIME_WAIT_SECONDS);
        WebElement inboxLink = driver.findElement(INBOX_LINK_LOCATOR);
        inboxLink.click();
        new WebDriverWait(driver, TIME_OUT_MAIL_ARRIVED_SECONDS).until(ExpectedConditions.visibilityOfElementLocated(By.xpath(String.format(INBOX_CHECKBOX_LOCATOR_PATTERN, mailSubject))));
        WebElement inboxCheckbox = driver.findElement(By.xpath(String.format(INBOX_CHECKBOX_LOCATOR_PATTERN, mailSubject)));
        inboxCheckbox.click();
        WebElement deleteButton = driver.findElement(DELETE_BUTTON_LOCATOR);
        deleteButton.click();
        WebElement deleteboxLink = driver.findElement(DELETEBOX_LINK_LOCATOR);
        deleteboxLink.click();
        new WebDriverWait(driver, TIME_OUT_MAIL_ARRIVED_SECONDS).until(ExpectedConditions.visibilityOfElementLocated(By.xpath(String.format(MAIL_LINK_LOCATOR_PATTERN, mailSubject))));
        WebElement deleteMailSubject = driver.findElement(By.xpath(String.format(MAIL_LINK_LOCATOR_PATTERN, mailSubject)));
        Assert.assertEquals(mailSubject, deleteMailSubject.getText());
        System.out.println("Deleted mail is in list");

    /*  WebElement deleteboxCheckbox = driver.findElement(By.xpath(String.format(DELETEBOX_CHECKBOX_LOCATOR_PATTERN, mailSubject)));
        deleteboxCheckbox.click();
        Thread.sleep(TIME_WAIT_SECONDS);
        WebElement countMailAfter = driver.findElement(COUNT_MAIL_LOCATOR);
        deleteButton.click();
        Thread.sleep(TIME_WAIT_SECONDS);
        WebElement countMailBefore = driver.findElement(COUNT_MAIL_LOCATOR);
        Assert.assertNotEquals(countMailAfter, countMailBefore);
        System.out.println("Mail is deleted"); */
    }

    @AfterClass(description = "Close browser")
    public void clearBrowser() {
        driver.close();
    }

    private WebElement waitForElement(By locator) {
        new WebDriverWait(driver, TIME_WAIT_SECONDS).until(ExpectedConditions.invisibilityOfElementLocated(locator));
        return driver.findElement(locator);
    }
}