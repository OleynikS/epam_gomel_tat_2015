package com.epam.gomel.sergey_oleynik.lesson4;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import java.util.concurrent.TimeUnit;

public class UnsuccessLoginToYandexMail {

    // AUT data
    public static final String YA_URL = "http://www.yandex.by";

    // UI data
    public static final By ENTER_BUTTON_LOCATOR = By.xpath("//button[contains(@class, 'button user')]");
    public static final By LOGIN_INPUT_LOCATOR = By.id("b-mail-domik-username11");
    public static final By PASSWORD_INPUT_LOCATOR = By.id("b-mail-domik-password11");
    public static final By SUBMIT_BUTTON_LOCATOR = By.xpath("//input[@value='Войти']");
    public static final By ERROR_LINK_LOCATOR = By.xpath("//div[@class='error-msg']");

    // Tools data
    public static final int DRIVER_PAGE_LOAD_TIMEOUT_SECONDS = 20;
    public static final int DRIVE_IMPL_WAIT_TIMEOUT_SECONDS = 5;
    private WebDriver driver;

    // Test data
    private String userLogin = "OSV.test"; // Account Login
    private String userPassword = "12345"; // Account password
    private String errorMessage = "Неправильная пара логин-пароль! Авторизоваться не удалось.";

    @BeforeClass(description = "Prepare browser")
    public void prepareBrowser() {
        driver = new FirefoxDriver();
        driver.manage().timeouts().pageLoadTimeout(DRIVER_PAGE_LOAD_TIMEOUT_SECONDS, TimeUnit.SECONDS);
        driver.manage().timeouts().implicitlyWait(DRIVE_IMPL_WAIT_TIMEOUT_SECONDS, TimeUnit.SECONDS);
    }

    @Test(description = "Unsuccess mail login")
    public void login() {
        driver.get(YA_URL);
        WebElement enterButton = driver.findElement(ENTER_BUTTON_LOCATOR);
        enterButton.click();
        WebElement loginInput = driver.findElement(LOGIN_INPUT_LOCATOR);
        loginInput.sendKeys(userLogin);
        WebElement passInput = driver.findElement(PASSWORD_INPUT_LOCATOR);
        passInput.sendKeys(userPassword);
        WebElement submitButton = driver.findElement(SUBMIT_BUTTON_LOCATOR);
        submitButton.click();
        WebElement expectedErrorMessage = driver.findElement(ERROR_LINK_LOCATOR);
        Assert.assertEquals(errorMessage, expectedErrorMessage.getText());
        System.out.println(errorMessage);
    }

    @AfterClass(description = "Close browser")
    public void clearBrowser() {
       driver.close();
    }
}
