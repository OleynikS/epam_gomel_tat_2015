package com.epam.gomel.sergey_oleynik.lesson5.pages;

import org.openqa.selenium.By;
import org.testng.Assert;

public class MailLoginPage extends AbstractBasePage {
    public static final String BASE_URL = "http://www.mail.yandex.by";
    public static final By LOGIN_INPUT_LOCATOR = By.id("b-mail-domik-username11");
    public static final By PASSWORD_INPUT_LOCATOR = By.id("b-mail-domik-password11");
    private static final By LOGIN_BUTTON_LOCATOR = By.xpath("//input[@type='submit']");
    public static final By ERROR_LINK_LOCATOR = By.xpath("//div[@class='error-msg']");

    public MailLoginPage open() {
        browser.open(BASE_URL);
        return this;
    }

    public MailBasePage login(String login, String password) {
        browser.type(LOGIN_INPUT_LOCATOR, login);
        browser.type(PASSWORD_INPUT_LOCATOR, password);
        browser.click(LOGIN_BUTTON_LOCATOR);
        return new MailBasePage();
    }

    public boolean isErrorPresent() {
       browser.waitForVisible(ERROR_LINK_LOCATOR);
       return true;
    }
}