package com.epam.gomel.sergey_oleynik.lesson5.pages;

import org.openqa.selenium.By;

public class MailTrashListPage extends AbstractBasePage {

    public static final String TRASH_MAIL_SUBJECT_LINK_LOCATOR_PATTERN = "//div[@class='block-messages' and not(@style)]//span[@title='%s']";

    public boolean isMessagePresent(String subject) {
        browser.waitForVisible(By.xpath(String.format(TRASH_MAIL_SUBJECT_LINK_LOCATOR_PATTERN, subject)));
        return true;
    }
}