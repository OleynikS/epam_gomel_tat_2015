package com.epam.gomel.sergey_oleynik.lesson5.pages;

import org.openqa.selenium.By;

public class MailSpamListPage extends AbstractBasePage {

    public static final String SPAM_MAIL_SUBJECT_LINK_LOCATOR_PATTERN = "//div[@class='block-messages' and not(@style)]//span[@title='%s']";
    public static final String SPAM_MAIL_CHECKBOX_LOCATOR_PATTERN = "//div[@class='block-messages' and not(@style)]//span[@title='%s']/ancestor::div[contains(@class,'b-messages__message')]//input";
    public static final By NOT_SPAM_BUTTON_LOCATOR = By.xpath("//a[@title='Не спам!']");

    public boolean isMessagePresent(String subject) {
        browser.waitForVisible(By.xpath(String.format(SPAM_MAIL_SUBJECT_LINK_LOCATOR_PATTERN, subject)));
        return true;
    }

    public boolean isMessageNotPresent(String subject) {
        browser.waitForAjaxProcessed();
//        browser.isPresent(By.xpath(String.format(SPAM_MAIL_SUBJECT_LINK_LOCATOR_PATTERN, subject)));
//        browser.waitForPresent(By.xpath(String.format(SPAM_MAIL_SUBJECT_LINK_LOCATOR_PATTERN, subject)));
        return browser.isPresent(By.xpath(String.format(SPAM_MAIL_SUBJECT_LINK_LOCATOR_PATTERN, subject)));
    }

    public MailBasePage markNotSpam (String subject) {
        browser.click(By.xpath(String.format(SPAM_MAIL_CHECKBOX_LOCATOR_PATTERN, subject)));
        browser.click(NOT_SPAM_BUTTON_LOCATOR);
        return new MailBasePage();
    }
}