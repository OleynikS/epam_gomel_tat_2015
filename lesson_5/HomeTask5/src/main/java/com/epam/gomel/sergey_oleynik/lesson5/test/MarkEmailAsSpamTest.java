package com.epam.gomel.sergey_oleynik.lesson5.test;

import com.epam.gomel.sergey_oleynik.lesson5.pages.MailBasePage;
import com.epam.gomel.sergey_oleynik.lesson5.pages.MailLoginPage;
import com.epam.gomel.sergey_oleynik.lesson5.ui.Browser;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;

public class MarkEmailAsSpamTest {
    // Test data
    private String userLogin = "OSV.test";
    private String userPassword = "OSV_test";
    private String mailTo = "OSV.test@yandex.ru";
    private String mailSubject = "Test subject " + Math.random() * 1000000;
    private String mailContent = "Mail content " + Math.random() * 1000000;

    @Test(description = "Mark Mail as Spam")
    public void markMailAsSpam() {

        MailBasePage mailbox = new MailLoginPage()
                .open()
                .login(userLogin, userPassword)
                .openInboxPage()
                .openComposeMailPage()
                .sendMail(mailTo, mailSubject, mailContent)
                .openInboxPage()
                .markSpam(mailSubject);
        Assert.assertTrue(mailbox.openSpamPage().isMessagePresent(mailSubject), "Mail in Spam list");
    }

    @AfterClass(description = "Close browser")
    public void clearBrowser() {
        Browser.get().close();
    }
}