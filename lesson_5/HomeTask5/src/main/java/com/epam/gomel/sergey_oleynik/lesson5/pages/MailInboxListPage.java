package com.epam.gomel.sergey_oleynik.lesson5.pages;

import org.openqa.selenium.By;

public class MailInboxListPage extends AbstractBasePage {

    public static final By COMPOSE_BUTTON_LOCATOR = By.xpath("//a[@href='#compose']");
    public static final String INBOX_MAIL_CHECKBOX_LOCATOR_PATTERN = "//div[@class='block-messages' and @style!='display: none;']//span[@title='%s']/ancestor::div[contains(@class,'b-messages__message')]//input";
    public static final String INBOX_MAIL_SUBJECT_LINK_LOCATOR_PATTERN = "//div[@class='block-messages' and @style!='display: none;']//span[@title='%s']";
    public static final By DELETE_MAIL_BUTTON_LOCATOR = By.xpath("//a[@title='Удалить (Delete)']");
    public static final String ACCOUNT_LINK_LOCATOR_PATTERN = "//a[@id='nb-1']/span[contains(.,'%s')]";
    public static final By SPAM_BUTTON_LOCATOR = By.xpath("//a[@title='Это спам! (Shift + s)']");

    public ComposeMailPage openComposeMailPage() {
        browser.click(COMPOSE_BUTTON_LOCATOR);
        return new ComposeMailPage();
    }

    public MailBasePage deleteInboxMail(String subject) {
        browser.click(By.xpath(String.format(INBOX_MAIL_CHECKBOX_LOCATOR_PATTERN, subject)));
        browser.click(DELETE_MAIL_BUTTON_LOCATOR);
        return new MailBasePage();
    }

    public boolean isMessagePresent(String text) {
        if (text.endsWith("@yandex.ru")) {
            browser.waitForVisible(By.xpath(String.format(ACCOUNT_LINK_LOCATOR_PATTERN, text)));
        }
        else { browser.waitForVisible(By.xpath(String.format(INBOX_MAIL_SUBJECT_LINK_LOCATOR_PATTERN, text))); }
        return true;
    }

    public MailBasePage markSpam (String subject) {
        browser.click(By.xpath(String.format(INBOX_MAIL_CHECKBOX_LOCATOR_PATTERN, subject)));
        browser.click(SPAM_BUTTON_LOCATOR);
        return new MailBasePage();
    }
}