package com.epam.gomel.sergey_oleynik.lesson5.pages;

import com.epam.gomel.sergey_oleynik.lesson5.ui.Browser;

public abstract class AbstractBasePage {

    protected Browser browser;

    public AbstractBasePage() {
        this.browser = Browser.get();
    }
}
