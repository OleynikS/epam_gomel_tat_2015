package com.epam.gomel.sergey_oleynik.lesson5.pages;

import org.openqa.selenium.By;

public class MailSentListPage extends AbstractBasePage {

    public static final String SENT_MAIL_SUBJECT_LINK_LOCATOR_PATTERN = "//div[@class='block-messages']//span[@title='%s']";

    public boolean isMessagePresent(String subject) {
        browser.waitForVisible(By.xpath(String.format(SENT_MAIL_SUBJECT_LINK_LOCATOR_PATTERN, subject)));
        return true;
    }
}