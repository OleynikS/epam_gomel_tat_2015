package com.epam.gomel.sergey_oleynik.lesson5.test;

import com.epam.gomel.sergey_oleynik.lesson5.pages.MailBasePage;
import com.epam.gomel.sergey_oleynik.lesson5.pages.MailLoginPage;
import com.epam.gomel.sergey_oleynik.lesson5.ui.Browser;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;

public class SuccessLoginToYandexTest {
    // Test data
    private String userLogin = "OSV.test";
    private String userPassword = "OSV_test";
    private String nameAccount = "OSV.test@yandex.ru";

    @Test(description = "Success login to Yandex")
    public void successLoginToYandex() {

        MailBasePage mailbox = new MailLoginPage()
                .open()
                .login(userLogin, userPassword);
        Assert.assertTrue(mailbox.openInboxPage().isMessagePresent(nameAccount), "Success login to yandex mail");
    }

    @AfterClass(description = "Close browser")
    public void clearBrowser() {
        Browser.get().close();
    }
}