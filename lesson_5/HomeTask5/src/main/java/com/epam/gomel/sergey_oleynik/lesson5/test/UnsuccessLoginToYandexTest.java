package com.epam.gomel.sergey_oleynik.lesson5.test;

import com.epam.gomel.sergey_oleynik.lesson5.pages.MailBasePage;
import com.epam.gomel.sergey_oleynik.lesson5.pages.MailLoginPage;
import com.epam.gomel.sergey_oleynik.lesson5.ui.Browser;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;

public class UnsuccessLoginToYandexTest {
    // Test data
    private String userLogin = "OSV.test";
    private String userPassword = "12345";

    @Test(description = "Unsuccess login to Yandex")
    public void unsuccessLoginToYandex() {

        MailBasePage mailbox = new MailLoginPage()
                .open()
                .login(userLogin, userPassword);
        Assert.assertTrue(mailbox.errorLoginPage().isErrorPresent(), "Unsuccess login to Yandex mail");
    }

    @AfterClass(description = "Close browser")
    public void clearBrowser() {
        Browser.get().close();
    }
}