package com.epam.gomel.sergey_oleynik.lesson5.pages;

import org.openqa.selenium.By;

public class MailBasePage extends AbstractBasePage {

    public static final By INBOX_LINK_LOCATOR = By.xpath("//a[@href='#inbox']");
    public static final By OUTBOX_LINK_LOCATOR = By.xpath("//a[@href='#sent']");
    public static final By TRASH_LINK_LOCATOR = By.xpath("//a[@href='#trash']");
    public static final By SPAM_LINK_LOCATOR = By.xpath("//a[@href='#spam']");

    public MailInboxListPage openInboxPage() {
        browser.click(INBOX_LINK_LOCATOR);
        return new MailInboxListPage();
    }

    public MailSentListPage openSentPage() {
        browser.click(OUTBOX_LINK_LOCATOR);
        return new MailSentListPage();
    }

    public MailTrashListPage openTrashPage() {
        browser.click(TRASH_LINK_LOCATOR);
        return new MailTrashListPage();
    }

    public MailSpamListPage openSpamPage() {
        browser.click(SPAM_LINK_LOCATOR);
        return new MailSpamListPage();
    }

    public MailLoginPage errorLoginPage() {
        return new MailLoginPage();
    }
}