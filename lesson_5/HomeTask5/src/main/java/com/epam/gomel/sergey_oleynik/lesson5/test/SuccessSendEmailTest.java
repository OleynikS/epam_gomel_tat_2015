package com.epam.gomel.sergey_oleynik.lesson5.test;

import com.epam.gomel.sergey_oleynik.lesson5.pages.MailBasePage;
import com.epam.gomel.sergey_oleynik.lesson5.pages.MailLoginPage;
import com.epam.gomel.sergey_oleynik.lesson5.ui.Browser;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;

public class SuccessSendEmailTest {
    // Test data
    private String userLogin = "OSV.test";
    private String userPassword = "OSV_test";
    private String mailTo = "OSV.test@yandex.ru";
    private String mailSubject = "Test subject " + Math.random() * 1000000;
    private String mailContent = "Mail content " + Math.random() * 1000000;
//    private static final String FILE_NAME = "D:\\_EPAM\\HomeTasks\\epam_gomel_tat_2015\\lesson_4\\HomeTask4\\attachFile.txt";

    @Test(description = "Send Mail")
    public void sendMail() {

        MailBasePage mailbox = new MailLoginPage()
                .open()
                .login(userLogin, userPassword)
                .openInboxPage()
                .openComposeMailPage()
                .sendMail(mailTo, mailSubject, mailContent);
        Assert.assertTrue(mailbox.openSentPage().isMessagePresent(mailSubject), "Mail is in sent list");
    }

    @AfterClass(description = "Close browser")
    public void clearBrowser() {
        Browser.get().close();
    }
}