package com.epam.gomel.sergey_oleynik.lesson5.pages;

import org.openqa.selenium.By;

public class ComposeMailPage extends AbstractBasePage {

    public static final By TO_INPUT_LOCATOR = By.xpath("//tr[contains(@class, 'field_to')]//input[@type='text']");
    public static final By SUBJECT_INPUT_LOCATOR = By.id("compose-subj");
    public static final By MAIL_TEXT_LOCATOR = By.id("compose-send");
    public static final By SEND_MAIL_BUTTON_LOCATOR = By.id("compose-submit");
    public static final By ATTACH_FILE_BUTTON_LOCATOR = By.xpath("//input[@name='att']");

    public MailBasePage sendMail(String mailTo, String mailSubject, String mailContent) {
        browser.type(TO_INPUT_LOCATOR, mailTo);
        browser.type(SUBJECT_INPUT_LOCATOR, mailSubject);
        browser.type(MAIL_TEXT_LOCATOR, mailContent);
        browser.click(SEND_MAIL_BUTTON_LOCATOR);
        browser.waitForAjaxProcessed();
        return new MailBasePage();
    }

    public MailBasePage sendMail(String mailTo, String mailSubject, String mailContent, String attach) {
        browser.type(TO_INPUT_LOCATOR, mailTo);
        browser.type(SUBJECT_INPUT_LOCATOR, mailSubject);
        browser.type(MAIL_TEXT_LOCATOR, mailContent);
        if(attach != null) {
            browser.attachFile(ATTACH_FILE_BUTTON_LOCATOR, attach);
        }
        browser.click(SEND_MAIL_BUTTON_LOCATOR);
        browser.waitForAjaxProcessed();
        // ждем появления элемента
        return new MailBasePage();
    }
}